//
//  ViewController.swift
//  assignment1
//
//  Created by JiYong KIM on 2017-01-10.
//  Copyright © 2017 JiYong KIM. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //let uname = "test"
    //let pass = "1234"
    
    @IBOutlet weak var user: UITextField!
    @IBOutlet weak var pw: UITextField!
    @IBOutlet weak var result: UILabel!
    
    @IBAction func submit(_ sender: UIButton) {
        // ALEX: Remove unused code to make code clean
        /*
        /////////////경고창 만들기/////////////////모든 명령어들 액션버튼 펑션 안으로.
        // 선언이 펑션 밖에, ok버튼이 펑션 안에 선언돼있으면 ok버튼이 계속 늘어남
        //alert 선언 및 만들기
        let 경고 = UIAlertController(title: "어이쿠", message: "메세지내용", preferredStyle: UIAlertControllerStyle.alert)
        // ok버튼 만들기
        경고.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        // 화면에 띄우기
         self.present(경고, animated: true, completion: nil)
        */

        // ALEX: Use full word instead of short one.
        var errorMessage: String = ""

        // ALEX: Use English not Korean for value, variable, comments, etc
        // Both empty
        if user.text == "" && pw.text == "" {
            errorMessage = "Username and Password are empty"
        }
        // Username is empty
        else if user.text == "" {
            errorMessage = "Username is empty"
        }
        // Password is empty
        else if pw.text == "" {
            errorMessage = "Password is empty"
        }
        
            // ALEX: Remove unused code to make code clean
           // if user.text == uname && pw.text == pass {
        
        // ALEX: I think this is better logic
        if errorMessage.isEmpty
        {
            // ALEX: If username and password are correct
            if user.text == "test" && pw.text == "1234" {
                result.text = "Success"
            }
            // ALEX: If username or passowrd is not correct
            else if user.text != "test" || pw.text != "1234" {
                result.text = "Failed"
            }
        } else {
            // ALEX: Create alert controller
            let alertController = UIAlertController(title: "Error", message: errorMessage, preferredStyle: UIAlertControllerStyle.alert)
            // ALEX: Add actionn to the alert controller
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            // ALEX: Presetn alert controller
            self.present(alertController, animated: true, completion: nil)
            
            result.text = ""
        }
    }
        
    // ALEX: Remove extra empty lines
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

